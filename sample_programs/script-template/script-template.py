## Import all dependencies
import os
import argparse
import sys
#import logging

## Define logging
#logging.basicConfig(format='%(message)s', level=logging.DEBUG, stream=sys.stdout)

parser = argparse.ArgumentParser()
parser.add_argument("square", help="display a square of a given number", type=int)
parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
args = parser.parse_args()
print(args.square**2)
if args.verbose:
    print("verbosity turned on")
